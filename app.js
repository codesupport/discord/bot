"use strict";

// Require local files.
const config = require("./config.json");
const utils = require("./utils.js");

// Require dependancies.
const fs = require("fs");
const discord = require("discord.js");
const firebase = require("firebase");

// Setup dependancies.
const client = new discord.Client({
	fetchAllMembers: true
});

firebase.initializeApp(config.firebase);
client.login(config.discord.token);

const database = firebase.firestore();

// Command and Event handlers.
var commands = {};

fs.readdir("./commands", (err, files) => {
	for (const file of files) {
		if (file.includes(".js")) {
			commands[file.replace(".js", "")] = require(`./commands/${file}`);
		}
	}
});

client.on("ready", () => {
	console.log("Bot started.");
});

client.on("message", (message) => {
	if (!message.content.startsWith(config.prefix)) return

	for (const cmd in commands) {
		const command = commands[cmd];
		const properties = command.properties;
		const args = message.content.split(" ");

		if (args[0].replace(config.prefix, "") == properties.command) {
			if (properties.arguments.length == 0) {
				command.run(message);
			} else {
				command.run(message, args);
			}
		}
	}
});

// Vote Muting Members
client.on("messageReactionAdd", (reaction, user) => {

	/*
	 * reaction - the reaction added to the message
	 * user		- the user who reacted
	 * message	- the message reacted to
	 * pointsCount - the amount of points on the message.
	 */

	var message = reaction.message;
	var pointsCount = 0;
	var reacter = message.guild.member(user);

	const mutedRole = message.guild.roles.find(role => role.name == "Muted");

	if (reaction.emoji.name == "shh") {
		if (reacter.roles.some(role => ["Verified", "Moderators", "Mod Assistant"].includes(role.name))) {
			pointsCount += 5;
		} else {
			pointsCount++;
		}

		if (pointsCount >= 5) {
			if (message.member.roles.some(role => ["Verified", "Moderators", "Mod Assistant"].includes(role.name))) {
				message.member.addRole(mutedRole, "This member has been vote-muted.");
			}
		}
	}
});

client.on("guildMemberUpdate", (oldMember, newMember) => {
	const mutedRole = newMember.guild.roles.find(role => role.name == "Muted");

	if (newMember.roles.some(role => role.name === "Muted")) {

		const guild = client.guilds.find("id", "240880736851329024");
		const channel = guild.channels.find("id", "405068878151024640");
		const embed = new Discord.RichEmbed();

		embed.setTitle(`Muted ${newMember.displayName}`);

		channel.send({ embed });

		setTimeout(() => {
			newMember.removeRole(mutedRole, "This users timeout is now over!");

			const guild = client.guilds.find("id", "240880736851329024");
			const channel = guild.channels.find("id", "405068878151024640");
			const embed = new Discord.RichEmbed();

			embed.setTitle(`Unmuted ${newMember.displayName}`);

			channel.send({ embed });

		}, 1000 * 60 * 20);
	}
});

// Logging Deleted Messages
client.on("messageDelete", (message) => {
	const guild = client.guilds.find("id", "240880736851329024");
	var channel = client.channels.get('405068878151024640');
	let author = message.author;
	let avatar = author.displayAvatarURL;

	let deletedMessage = new Discord.RichEmbed()
		.setTitle('Message deleted')
		.setAuthor(author.username, avatar)
		.addField("Channel", message.channel, true)
		.addField("Content", message.content, true)
		.setColor("#d62424");

	channel.send(deletedMessage);

});

client.on("messageUpdate", (oldMessage, newMessage) => {
	if (oldMessage.author.bot) return;
	var channel = client.channels.get('405068878151024640');
	let author = newMessage.author;
	let avatar = author.displayAvatarURL;

	let updateMessage = new Discord.RichEmbed()
		.setTitle('Message updated')
		.setAuthor(author.username, avatar)
		.addField("Original", oldMessage.cleanContent, true)
		.addField("Edit", newMessage.cleanContent, true)
		.setColor("#00a1ff");

	channel.send(updateMessage);
});

client.on("guildMemberAdd", (member) => {
	var channel = client.channels.get('405068878151024640');
	let memberCount = client.members;

	let newMember = new Discord.RichEmbed()
		.setTitle('New member has joined')
		.setAuthor(member.username, member.displayAvatarURL)
		.addField("Member count", memberCount)
		.setColor("#1cc936");

	channel.send(newMember);
});

cobalt.on("guildMemberRemove", (member) => {
	var channel = cobalt.channels.get('405068878151024640');
	let memberCount = cobalt.members;

	let newMember = new Discord.RichEmbed()
		.setTitle('member has left or got banned')
		.setAuthor(member.username, member.displayAvatarURL)
		.addField("member count", memberCount)
		.setColor("d62424");

	channel.send(newMember);
});


// Export tools
exports.config = config;
exports.utils = utils;
exports.discord = discord;
exports.client = client;
exports.firebase = firebase;
exports.database = database;