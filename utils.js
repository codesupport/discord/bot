"use strict";

const app = require("./app.js");

const config = app.config;
const discord = app.discord;

/*
* Multiple Line Message
* Turns each index of an array into a new line.
* @param {array} lines A list of lines to be seperated into new lines.
* @returns {string} The string of multiple lines.
*/
function multiLineMessage(lines) {
	var msg = "";
	for (const line in lines) {
		msg += lines[line] + "\n";
		if (line == (lines.length - 1)) {
			return msg;
		}
	}
}

/*
* Capitalise First Letter
* Capitalises the first letter of a string.
* @param {string} string The string to be capitalised.
* @returns {string} The capitalised string.
*/
function capFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/*
* Error Message Embed
* Generates an error message embed.
* @param {string} message The error message to be displayed.
* @returns {object} The RichEmbed object.
*/
function errorMessage(message) {
	const embed = new app.discord.RichEmbed();

	embed.setTitle("Error");
	embed.setColor(app.config.embed.error_colour);
	embed.setDescription(message);

    return embed;
}

// Exports
exports.multiLineMessage = multiLineMessage;
exports.capFirst = capFirst;
exports.errorMessage = errorMessage;
