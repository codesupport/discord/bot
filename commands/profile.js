"use strict";

// Requirements
const app = require("./../app.js");
const config = app.config;
const utils = app.utils;
const discord = app.discord;
const client = app.client;
const firebase = app.firebase;
const database = app.database;
const prefix = config.prefix + "profile";

// Details about the command.
const properties = {
  command: "profile",
  description: "Manage your CodeSupport profile.",
  visible: true,
  arguments: ["<@mention|set|add>", "[field]", "[value]"]
}

// Runs when the command is triggered.
function run(message, args) {
  if (typeof args[1] == "undefined") {
    getProfile(message, message.author.id);
  } else {
    if (["set", "add", "help"].includes(args[1])) {
      switch (args[1]) {
        case "set":
          let value = "";
          for (let i = 0; i < args.length; i++) {
            if (i === 0 || i === 1 || i === 2) continue;
            if (i === (args.length - 1)) {
              value += args[i];
            } else {
              value += args[i] + " ";
            }
          }
          setProfile(message, args[2], value);
          break;
        case "add":
          addProfile(message, args[2], args[3]);
          break;
        case "help":
          message.channel.send(getHelp());
      }
    } else {
      let id;
      //Checks if there was a ping in the message
      if (message.mentions.users.first()) {
        id = message.mentions.users.first().id;
      }
      //Checks if there is a user id in the message
      else {
        id = args[1];
      }
      getProfile(message, id);
    }
  }
}

function createProfile(user) {
  const userRef = database.collection("users").doc(user.id);
  userRef.set({
    username: user.username
  }, {
      merge: true
    })
}

// Add Profile
function addProfile(message, field, value) {
  const userRef = database.collection("users").doc(message.author.id);
  if (!userRef.exists) {
    createProfile(message.author);
  }
  if (field == "language") {
    userRef.update({
      languages: firebase.firestore.FieldValue.arrayUnion(value)
    }).then(() => {
      const embed = new discord.RichEmbed();

      embed.setTitle("Profile Updated");
      embed.setColor(config.embed.success);
      embed.setDescription(`
				${utils.capFirst(value)} has been added to your known languages.
			`);

      message.channel.send({ embed });

    }).catch((error) => {
      message.channel.send(utils.errorMessage(`
				An error occured whilst interacting with the database (${error.code}).
			`));
    });
  } else {
    message.channel.send(utils.errorMessage(`
			Hmm... that doesn't seem to be a valid field.\n
			Accepted field: \`language\`.
		`));
  }
}

// Set Profile
function setProfile(message, field, value, secondValue) {
  if (["git", "bio", "country"].includes(field)) {
    const userRef = database.collection("users").doc(message.author.id);

    userRef.set({
      username: message.author.username,
      [field]: value
    }, {
        merge: true
      }).then(() => {
        const embed = new discord.RichEmbed();

        embed.setTitle("Profile Updated");
        embed.setColor(config.embed.success);
        embed.setDescription(`
				${utils.capFirst(field)} has been set to "${value}".
			`);

        message.channel.send({ embed });
      }).catch((error) => {
        message.channel.send(utils.errorMessage(`
				An error occured whilst interacting with the database (${error.code}).
			`));
      });
  } else {
    if (field == "languages") {
      message.channel.send(utils.errorMessage(`
				You can add a languge by doing \`${prefix} add language <language>\`.
			`));
    } else {
      message.channel.send(utils.errorMessage(`
				Hmm... that doesn't seem to be a valid field.\n
				Accepted fields: \`git\`, \`bio\` and \`country\`.
			`));
    }
  }
}

// Get Profile
function getProfile(message, user) {
  const userRef = database.collection("users").doc(user);

  userRef.get().then((doc) => {
    if (doc.exists) {
      const embed = new discord.RichEmbed();
      const data = doc.data();
      const guild = client.guilds.find("id", config.guild);
      const discordProfile = guild.members.find("id", user);

      embed.setTitle(`${data.username}'s Profile`);
      embed.setColor(discordProfile.displayHexColor);
      embed.setDescription(data.bio == undefined ? "" : data.bio);

      if (typeof data.languages != "undefined") {
        const languagesArray = data.languages;
        var languagesString = "";

        languagesArray.forEach((language, index) => {
          if (languagesArray.length == 1) {
            languagesString = language;
          } else if ((index == languagesArray.length - 1) && languagesArray.length >= 2) {
            languagesString += ` and ${language}.`;
          } else if ((index == languagesArray.length - 2) && languagesArray.length >= 2) {
            languagesString += language;
          } else {
            languagesString += `${language}, `;
          }
        });

        embed.addField("Known Languages", languagesString);
      }

      if (typeof data.git != "undefined") {
        embed.addField("Git", data.git);
      }

      if (typeof data.country != "undefined") {
        embed.addField("Country", data.country);
      }

      var month = "";
      var day = discordProfile.joinedAt.getDate();
      var year = discordProfile.joinedAt.getFullYear();

      switch (discordProfile.joinedAt.getMonth()) {
        case 0: month = "January"; break;
        case 1: month = "February"; break;
        case 2: month = "March"; break;
        case 3: month = "April"; break;
        case 4: month = "May"; break;
        case 5: month = "June"; break;
        case 6: month = "July"; break;
        case 7: month = "August"; break;
        case 8: month = "September"; break;
        case 9: month = "October"; break;
        case 10: month = "November"; break;
        case 11: month = "December"; break;
        default: month = discordProfile.joinedAt.getMonth();
      }

      embed.addField("Join Date", `${month} ${day}, ${year}`);

      message.channel.send({ embed });
    } else {

      createProfile(message.author);
      getProfile(message, message.author);
      if (user === message.author.id) {
        message.channel.send(getHelp());
        return;
      }
      message.channel.send(utils.errorMessage(`
				Hmm... It appears that user does not exist.
			`));
    }
  }).catch((error) => {
    console.log(error);
    message.channel.send(utils.errorMessage(`
			An error occured whilst interacting with the database (${error.code}).
		`));
  });
}

function getHelp() {
  const embed = new discord.RichEmbed();

  embed.addField(`${prefix} add language <language>`, `Adds a language to your **${prefix}**`);
  embed.addField(`${prefix} set bio <bio>`, `Sets the bio of your **${prefix}**`);
  embed.addField(`${prefix} set git <github/gitlab link>`, `Sets the git account of your **${prefix}**`);
  embed.addField(`${prefix} set country <country>`, `Sets the country of your **${prefix}**`);

  return embed;
}

exports.properties = properties;
exports.run = run;

// !profile set git <service>
